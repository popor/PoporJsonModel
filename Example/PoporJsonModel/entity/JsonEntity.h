//
//  JsonEntity.h
//  PoporJsonModel_Example
//
//  Created by popor on 2022/1/21.
//  Copyright © 2022 popor. All rights reserved.
//

#import <PoporJsonModel/PoporJsonModel.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JsonEntity_sub2;
@interface JsonEntity_sub2 : PoporJsonModel

@property (nonatomic, copy  ) NSString * name;

@end

@protocol JsonEntity_sub;
@interface JsonEntity_sub : PoporJsonModel

@property (nonatomic, copy  ) NSString * name;
@property (nonatomic, copy  ) NSArray<JsonEntity_sub2> * array;

@end

@interface JsonEntity : PoporJsonModel

// copy 属性在异常捕捉class会出错, 最好为strong的NSMutableArray.
@property (nonatomic, copy  ) NSArray<JsonEntity_sub> * array;

@property (nonatomic, strong) NSMutableArray<JsonEntity_sub> * array_Strong;
@property (nonatomic, strong) NSMutableAttributedString * text;

@end

NS_ASSUME_NONNULL_END
