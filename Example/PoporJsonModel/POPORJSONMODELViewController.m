//
//  POPORJSONMODELViewController.m
//  PoporJsonModel
//
//  Created by popor on 11/01/2019.
//  Copyright (c) 2019 popor. All rights reserved.
//

#import "POPORJSONMODELViewController.h"

#import "PoporJsonModelConfig.h"

#import "JsonEntity.h"

@interface POPORJSONMODELViewController ()

@end

@implementation POPORJSONMODELViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set
    [PoporJsonModelConfig config];
    
    
    // test
    NSDictionary * dic = @{
        @"array":@[
            @{
                @"name":@"a",
                @"array":@[@{@"name":@"name1"}]
            },
            @{
                @"name":@"b",
                @"array":@[@{@"name":@"name1"}]
            },
            @{
                @"name":@"c",
                @"array":@[@{@"name":@"name1"}]
            }]
    };
    JsonEntity * entity = [[JsonEntity alloc] initWithDictionary:dic error:nil];
    
    //NSLog(@"entity: %@", entity.description);
    
}


@end
