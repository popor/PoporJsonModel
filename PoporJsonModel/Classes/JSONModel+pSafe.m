//
//  JSONModel+pSafe.m
//  JSONModel
//
//  Created by apple on 2019/11/1.
//

#import "JSONModel+pSafe.h"

#import "PoporJsonModelTool.h"
#import <objc/runtime.h>

@implementation JSONModel (pSafe)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // 交换方法, 尽量不再依赖外部class.
        [objc_getClass("JSONModel") SwizzleInstanceMethod_poporJsonModel:@selector(initWithDictionary:error:)
                                                              withMethod:@selector(safeInitWithDictionary:error:)];
    });
}

- (instancetype)safeInitWithDictionary:(NSDictionary *)dict error:(NSError **)err {
    id entity;
    if (err) {
        entity = [self safeInitWithDictionary:dict error:err];
    }else{
        NSError * error;
        entity = [self safeInitWithDictionary:dict error:&error];
        if (error) {
            PoporJsonModelTool * tool = [PoporJsonModelTool share];
            if (tool.blockError) {
                tool.blockError(error);
            } else {
                NSLog(@"PoporJsonModel ERROR: %@", error.userInfo[@"kJSONModelTypeMismatch"]);
            }
        }
        
#ifndef __OPTIMIZE__ //测试
        {   // 测试版本检查 copy类型的array, 否则一些oc的safe函数捕捉不到
            PoporJsonModelTool * tool = [PoporJsonModelTool share];
            if (tool.debugModel_checkCopyArray) {
                [self.class NSLogEntity:entity];
            }
        }
#else //正式
        
#endif
    }
    
    return entity;
}

+ (void)NSLogEntity:(NSObject * _Nullable)theClassEntity {
    
    if (!theClassEntity) { return; }
    
    NSString * copyText = @"\",C,N,V_";
    
    unsigned propertyCount;
    
    NSString * classString = NSStringFromClass([theClassEntity class]);
    NSString * propNameString;
    NSString * propAttributesString;
    
    objc_property_t *properties = class_copyPropertyList([theClassEntity class],&propertyCount);
    for(NSInteger i=0;i<propertyCount;i++){
        
        objc_property_t prop=properties[i];
        
        const char * propAttributes=property_getAttributes(prop);
        propAttributesString =[NSString stringWithCString:propAttributes encoding:NSASCIIStringEncoding];
        
        if ([propAttributesString rangeOfString:copyText].location != NSNotFound) {
            const char *propName = property_getName(prop);
            propNameString =[NSString stringWithCString:propName encoding:NSASCIIStringEncoding];
            
            // NSLog(@"propAttributesString: %@", propAttributesString);
            NSLog(@"⚠️⚠️ %@.%@ should not be copy NSArray ⚠️⚠️", classString, propNameString);
        }
        
    } // end for.
    free(properties);
}

#pragma mark - 交换方法
+ (void)SwizzleInstanceMethod_poporJsonModel:(SEL _Nonnull)originalSelector
                                  withMethod:(SEL _Nonnull)swizzledSelector
{
    if (!originalSelector || !swizzledSelector) {
        NSLog(@"交换方法失败! %s", __func__);
        return;
    }
    
    Class class           = [self class];
    Method originalMethod = class_getInstanceMethod(class, originalSelector); //原有方法
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector); //替换原有方法的新方法
    
    if (!originalMethod || !swizzledMethod) {
        NSLog(@"交换方法失败! %s", __func__);
        return;
    }
    
    //先尝试給源SEL添加IMP，这里是为了避免源SEL没有实现IMP的情况
    BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    if (didAddMethod) {//添加成功：说明源SEL没有实现IMP，将源SEL的IMP替换到交换SEL的IMP
        class_replaceMethod(class,swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    } else {//添加失败：说明源SEL已经有IMP，直接将两个SEL的IMP交换即可
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}


@end
